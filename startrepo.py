import os
from urllib.parse import urlparse
from typing import Union
import string
import gitlab
import requests
from bs4 import BeautifulSoup  # type: ignore
from gitlab.v4.objects import Project

gl: Union[None, gitlab.Gitlab]
gl = None


def _question_title_to_project_name(title: str) -> str:
    """
    Turn a title into a valid project name
    """
    # TODO: allow emojis?
    valid_characters = string.ascii_letters + string.digits + '_.-+ '
    s = ''.join(c for c in title if c in valid_characters)
    if not s:
        raise Exception
    if s[0] not in string.ascii_letters + string.digits:
        s = 'q ' + s
    while s[-1] not in string.ascii_letters + string.digits:
        s = s[:-1]
    for char in '_.-+':
        consecutive = char * 2
        while consecutive in s:
            s = s.replace(consecutive, char)
    return s


def _get_question_title(url: str) -> str:
    resp = requests.get(url)
    resp.raise_for_status()
    soup = BeautifulSoup(resp.text, 'lxml')
    title = soup.find('head').find('title').text.strip()
    title = title.rstrip(' - Stack Overflow')
    return title


def create_project_from_question_url(url: str, parent_group_id: str, visibility: str = 'private') -> str:
    if gl is None:
        raise Exception("gitlab instance is not configured")
    parsed = urlparse(url)
    path = parsed.path
    _, q, question_id, *__ = path.split('/')
    assert q == 'questions', 'Malformed url'
    title = _get_question_title(url)
    project_name = _question_title_to_project_name(title)
    group = gl.groups.get(parent_group_id)
    p: Project
    p = gl.projects.create(  # type: ignore
        dict(
            name=project_name,
            path=question_id,
            namespace_id=parent_group_id,
            initialize_with_readme=True,
            visibility=visibility
        )
    )
    return p.web_url


def main():
    global gl
    import argparse

    parser = argparse.ArgumentParser('start-question-repo')
    parser.add_argument(
        '--parent-group-id',
        help='The ID of the namespace where to create the project. Or set the START_QUESTION_REPO_PARENT_GROUP_ID environment variable',
    )
    parser.add_argument(
        '--private-token', help='The private GitLab API token. Or set the GITLAB_PRIVATE_TOKEN environment variable'
    )
    parser.add_argument(
        '--visibility',
        help='The visibility level of the project',
        default='private',
        choices=('private', 'internal', 'public'),
    )
    parser.add_argument(
        '--server-url', help='The base URL (including the scheme) of the gitlab server', default='https://gitlab.com'
    )
    parser.add_argument('stackoverflow_url', help='The URL of the stackoverflow question')

    args = parser.parse_args()

    private_token = args.private_token or os.environ['GITLAB_PRIVATE_TOKEN']
    parent_group_id = args.parent_group_id or os.environ.get('START_QUESTION_REPO_PARENT_GROUP_ID') or '54973647'
    gl = gitlab.Gitlab(args.server_url, private_token=private_token)
    repo = create_project_from_question_url(
        args.stackoverflow_url, parent_group_id=parent_group_id, visibility=args.visibility
    )
    print('Repo created:', repo)


if __name__ == '__main__':
    main()
