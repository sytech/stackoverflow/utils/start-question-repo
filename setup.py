from setuptools import setup

setup(
    name='start-question-repo',
    version='0.1.0',
    url='https://gitlab.com/spyoungtech/stackoverflow/utils/start-question-repo',
    license='MIT',
    author='Spencer Phillip Young',
    author_email='spencer.young@spyoung.com',
    description='CLI utility to create GitLab repos for testing SO questions',
    long_description='CLI utility to create GitLab repos for testing SO questions',
    install_requires=['python-gitlab', 'requests', 'lxml', 'bs4'],
    py_modules=['startrepo'],
    entry_points={'console_scripts': ['start-question-repo = startrepo:main']},
    platforms='any',
    classifiers=[
        'Programming Language :: Python :: 3',
    ],
)
