# start-question-repo

CLI utility to create repos for stackoverflow questions

Installation:

```bash
pip install --extra-index-url="https://gitlab.com/api/v4/projects/37448151/packages/pypi/simple" start-question-repo
```

Usage:

```
usage: start-question-repo [-h] [--parent-group-id PARENT_GROUP_ID] [--private-token PRIVATE_TOKEN] [--visibility {private,internal,public}] [--server-url SERVER_URL]
                           stackoverflow_url

positional arguments:
  stackoverflow_url     The URL of the stackoverflow question

optional arguments:
  -h, --help            show this help message and exit
  --parent-group-id PARENT_GROUP_ID
                        The ID of the namespace where to create the project. Or set the START_QUESTION_REPO_PARENT_GROUP_ID environment variable
  --private-token PRIVATE_TOKEN
                        The private GitLab API token. Or set the GITLAB_PRIVATE_TOKEN environment variable
  --visibility {private,internal,public}
                        The visibility level of the project
  --server-url SERVER_URL
                        The base URL (including the scheme) of the gitlab server```
```

Example:

```
$ export GITLAB_PRIVATE_TOKEN=glpat_abc123
$ start-question-repo https://stackoverflow.com/questions/12345678
Repo created: https://gitlab.com/sytech/stackoverflow/12345678
```
